<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends AbstractController
{
    /**
     * @Route("categories", methods={"GET"})
     */
    public function findAll()
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString($serializer->serialize($categories, 'json'));
    }

    /**
     * @Route("categories/{id}", methods={"GET"})
     */
    public function findOne(int $id)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($id);
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString($serializer->serialize($category, 'json'));
    }

    /**
     * @Route("categories", methods={"POST"})
     */
    public function addCategory(Request $req)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $category = $serializer->deserialize(
            $req->getContent(),
            Category::class,
            'json'
        );
        $manager->persist($category);
        $manager->flush();
    }

    /**
     * @Route("categories/{id}", methods={"PUT"})
     */
    public function updateCategory(Request $req, Category $category)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $update = $serializer->deserialize(
            $req->getContent(),
            Category::class,
            'json'
        );
        $category->setName($update->getName());
        $category->setLft($update - getLft());
        $category->setRgt($update->getRgt());
        $category->setLevel($update->getLevel());

        $manager->persist($category);
        $manager->flush();
    }

    /**
     * @Route("categories/{id}", methods={"DELETE"})
     */
    public function deleteOne(Category $category)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($category);
        $manager->flush();

        return new Response("Category deleted", 204);
    }
}
