<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PasswordRepository;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Password;
use App\Generator\PasswordGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


class PasswordController extends Controller
{
    /**
     * @Route("passwords", methods={"GET"})
     */
    public function findAll()
    {
        $passwords = $this->getDoctrine()
            ->getRepository(Password::class)
            ->findAll();
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString($serializer->serialize($passwords, 'json'));
    }

    /**
     * @Route("passwords/{id}", methods={"GET"})
     */
    public function findOne(int $id)
    {
        $password = $this->getDoctrine()
            ->getRepository(Password::class)
            ->find($id);
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString($serializer->serialize($password, 'json'));
    }

    /**
     * @Route("passwords?category={id}", methods={"GET"})
     */
    public function findByCat(int $id)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($id);

        $passwords = $category->getPasswords();
        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString($serializer->serialize($passwords, 'json'));
    }

    /**
     * @Route("password/proposals", methods={"POST"})
     */
    public function passwordProposal(Request $req)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $generator = $serializer->deserialize(
            $req->getContent(),
            PasswordGenerator::class,
            'json'
        );
        $password = $generator->generatePassword();
        $manager->persist($password);
        $manager->flush();
    }

    /**
     * @Route("passwords", methods={"POST"})
     */
    public function addPassword(Request $req)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $password = $serializer->deserialize(
            $req->getContent(),
            Password::class,
            'json'
        );
        $manager->persist($password);
        $manager->flush();
    }

    /**
     * @Route("passwords/{id}", methods={"PUT"})
     */
    public function updatePassword(Request $req, Password $password)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $update = $serializer->deserialize(
            $req->getContent(),
            Password::class,
            'json'
        );
        $password->setDescription($update->getDescription());
        $password->setName($update->getName());
        $password->setPassword($update->getPassword());
        $password->setUrl($update->getDescription());
        $password->setUsername($update->getUsername());

        $manager->persist($password);
        $manager->flush();
    }

    /**
     * @Route("passwords/{id}", methods={"DELETE"})
     */
    public function deleteOne(Password $password)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($password);
        $manager->flush();

        return new Response("Password deleted", 204);
    }
}
