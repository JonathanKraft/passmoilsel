<?php

namespace App\Generator;

class PasswordGenerator
{
  public function __construct(int $length, float $numerals, float $specialChars, bool $pronounceable) {
    $this->length = $length; 
    $this->numerals = $numerals; 
    $this->specialChars = $specialChars; 
    $this->pronounceable = $pronounceable;

    $this->tabNum = range(0, 9);
    $this->tabSpecialChar = ['!', '@', '#', '$', '%', '^', '&', '*', '-', '_'];
  }

  function generatePassword()
  {
    return $this->pronounceable ? $this->generatePronounceablePassword($this->length) : $this->generateNonPronounceablePassword($this->length, $this->numerals, $this->specialChars);
  }

  function generateNonPronounceablePassword(): string
  {
    $this->tabNum = range(0, 9);
    $this->tabSpecialChar = ['!', '@', '#', '$', '%', '^', '&', '*', '-', '_'];
    $tabAlpha = array_merge(
      range('a', 'z'), 
      range('A', 'Z')
    );
    $nbNum = intval($this->length * $this->numerals);
    $nbSpecialChar = intval($this->length * $this->specialChars);

    $nbAlpha = $this->length - $nbNum - $nbSpecialChar;

    $password = array_merge(
      $this->filler($tabAlpha, $nbAlpha),
      $this->filler($this->tabNum, $nbNum),
      $this->filler($this->tabSpecialChar, $nbSpecialChar)
    );

    shuffle($password);
    return implode('', $password);
  }

  function generatePronounceablePassword()
  {

    $this->tabNum = range(0, 9);
    $this->tabSpecialChar = ['!', '@', '#', '$', '%', '^', '&', '*'];
    $tabVowels = ['a', 'e', 'i', 'o', 'u', 'y'];
    $tabConsanant = ['z', 'r', 't', 'p', 'q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'w', 'x', 'c', 'v', 'b', 'n'];

    $password = [];
    for ($i = 0; $i <= $this->length; $i++) {
      $nextWordLength = random_int(3, 8);

      for ($iterator = 0; $iterator < $nextWordLength; $iterator++) {
        $password[] = $this->randomChar(($iterator % 2 === 0)? $tabVowels : $tabConsanant);
        if ($iterator === $nextWordLength - 1) {
          $password[] = $this->randomChar($this->tabNum);
          $password[] = $this->randomChar($this->tabSpecialChar);
        }
      }
    }
    return implode('', $password);
  }

  function filler(array $tab, int $nbChar): array
  {
    $result = [];
    for ($i = 0; $i < $nbChar; $i++) {
      $result[] = $this->randomChar($tab);
    }
    return $result;
  }

  function randomChar(array $characters): string {
    return $character = $characters[rand(0, count($characters) - 1)];
  }
}
