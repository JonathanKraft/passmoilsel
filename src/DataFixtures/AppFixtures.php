<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;
use App\Entity\Tag;
use App\Entity\Password;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $catArray = [];
        $tagArray = [];

        for ($i=0; $i < 6; $i++) { 
            $cat = new Category();
            $cat->setName("cat n°{$i}");
            $cat->setLft($i);
            $cat->setRgt($i);
            $cat->setLevel($i);

            $catArray[] = $cat;

            $manager->persist($cat);
            $manager->flush();
        }

        for ($i=0; $i < 11; $i++) { 
            $tag = new Tag();
            $tag->setName("tag n°{$i}");

            $tagArray[] = $tag;

            $manager->persist($tag);
        }

        for ($i=0; $i < 7; $i++) { 
            $password = new Password();
            $password->setName("passwordName n°{$i}");
            $password->setUrl("url n°{$i}");
            $password->setPassword("password{$i}");
            $password->setUsername("username{$i}");
            $password->getTags($tagArray[rand (0, 10)]);
            $password->setCategory($catArray[rand (0, 5)]);
            $password->setDescription("description{$i}");

            $manager->persist($password);
        }

        $manager->flush();
    }
}
